﻿
namespace PomodoroApp2 {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.lblWork = new System.Windows.Forms.Label();
            this.lblRest = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.tbWork = new System.Windows.Forms.TextBox();
            this.tbRest = new System.Windows.Forms.TextBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lblWork
            // 
            this.lblWork.AutoSize = true;
            this.lblWork.Location = new System.Drawing.Point(83, 45);
            this.lblWork.Name = "lblWork";
            this.lblWork.Size = new System.Drawing.Size(27, 15);
            this.lblWork.TabIndex = 0;
            this.lblWork.Text = "Rad";
            // 
            // lblRest
            // 
            this.lblRest.AutoSize = true;
            this.lblRest.Location = new System.Drawing.Point(381, 45);
            this.lblRest.Name = "lblRest";
            this.lblRest.Size = new System.Drawing.Size(45, 15);
            this.lblRest.TabIndex = 1;
            this.lblRest.Text = "Odmor";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblStatus.Location = new System.Drawing.Point(52, 173);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(399, 75);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "25:00";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbWork
            // 
            this.tbWork.Location = new System.Drawing.Point(52, 74);
            this.tbWork.Name = "tbWork";
            this.tbWork.Size = new System.Drawing.Size(100, 23);
            this.tbWork.TabIndex = 3;
            this.tbWork.Text = "25";
            // 
            // tbRest
            // 
            this.tbRest.Location = new System.Drawing.Point(351, 74);
            this.tbRest.Name = "tbRest";
            this.tbRest.Size = new System.Drawing.Size(100, 23);
            this.tbRest.TabIndex = 4;
            this.tbRest.Text = "5";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(52, 343);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(118, 50);
            this.btnStartStop.TabIndex = 5;
            this.btnStartStop.Text = "Start / Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(338, 343);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(129, 50);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 450);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.tbRest);
            this.Controls.Add(this.tbWork);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblRest);
            this.Controls.Add(this.lblWork);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWork;
        private System.Windows.Forms.Label lblRest;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox tbWork;
        private System.Windows.Forms.TextBox tbRest;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Timer timer1;
    }
}

