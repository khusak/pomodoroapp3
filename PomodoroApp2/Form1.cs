﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PomodoroApp2 {
    public partial class Form1 : Form {
        Pomodoro pomodoro;

        public Form1() {
            pomodoro = new Pomodoro();
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            timer1.Enabled = !timer1.Enabled;
        }

        private void timer1_Tick(object sender, EventArgs e) {
            pomodoro.Tick();
            UpdateStatus();
        }

        private void btnReset_Click(object sender, EventArgs e) {
            timer1.Enabled = false;
            try {
                int minutesWork = int.Parse(tbWork.Text);
                int minutesRest = int.Parse(tbRest.Text);
                pomodoro = new Pomodoro(minutesWork, minutesRest);
            } catch (FormatException) {
                MessageBox.Show("Upisali ste neispravnu vrijednost!");
            }
            UpdateStatus();
        }

        private void UpdateStatus() {
            //lblStatus.Text = pomodoro.CurrentTime();
            lblStatus.Text = pomodoro.ToString();
        }
    }
}
