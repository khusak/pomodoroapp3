﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PomodoroApp2 {
    class Pomodoro {
        public Pomodoro() : this(25, 5) {
        }

        public Pomodoro(int minutesWork, int minutesRest) {
            secondsWork = minutesWork * 60;
            secondsRest = minutesRest * 60;
            Seconds = secondsWork;
            InWork = true;
        }

        public void Tick() {
            Seconds--;
            if (Seconds == 0) {
                if (InWork) {
                    SetSecondsAndWork(secondsRest, false);
                } else {
                    SetSecondsAndWork(secondsWork, true);
                }
            }
        }

        public override string ToString() {
            return CurrentTime();
        }

        public string CurrentTime() {
            return string.Format("{0}:{1}", Seconds / 60, Seconds % 60);// 25:00
        }

        private void SetSecondsAndWork(int seconds, bool inWork) {
            Seconds = seconds;
            InWork = inWork;
        }

        public int Seconds { get; set; }
        public bool InWork { get; set; }

        private int secondsWork;
        private int secondsRest;
    }
}
